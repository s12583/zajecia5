package stream;

import stream.domain.*;
import stream.service.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Supplier;


/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {




      User b = Builder.of(User::new)
              .with(User::setName,"S12583")
              .with(User::setPassword,"tajne")
              .with(User::setPersonDetails,
                      Builder.of(Person::new)
                              .with(Person::setName,"Grzegorz")
                              .with(Person::setSurname,"Kowalski")
                              .with(Person:: setAge, 44)
                              .with(Person::setRole,Builder.of(Role::new)
                                      .with(Role::setName,"Pracownik")
                                      .withList(Role::setPermissions,Builder.of(Permission::new)
                                              .with(Permission::setName,"Admin")
                                              .build(),
                                              Builder.of(Permission::new)
                                                      .with(Permission::setName,"Root")
                                                      .build())

                                      .build())
                              .withList(Person::setPhoneNumbers,"739555697","839555699")
                              .withList(Person::setAddresses,
                                      Builder.of(Address::new)
                                              .with(Address::setStreetName,"Kochanowskiego")
                                              .with(Address::setHouseNumber,101)
                                              .with(Address:: setFlatNumber,12)
                                              .with(Address::setPostCode,"82-200")
                                              .with(Address:: setCountry,"Poslska")
                                              .with(Address::setCity,"Malbork")
                                              .build(),
                                      Builder.of(Address::new)
                                              .with(Address::setStreetName,"Grunwaldzka")
                                              .with(Address::setHouseNumber,11)
                                              .with(Address:: setFlatNumber,1456)
                                              .with(Address::setPostCode,"80-200")
                                              .with(Address:: setCountry,"Polska")
                                              .with(Address::setCity,"Gdańsk")
                                              .build())
                      .build())

              .build();



        System.out.print(b
        );

    }

}
