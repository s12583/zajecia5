package stream.domain;

/**
 * Created by Krzysztof Pawlowski on 22/11/15.
 */
public class Permission {
    private String name;

    @Override
    public String toString()
    {
        return "Permission{" + "name=" + name + '}';
    }
    
    

    public String getName() {
        return name;
    }

    public Permission setName(String name) {
        this.name = name;
        return this;
    }
}
