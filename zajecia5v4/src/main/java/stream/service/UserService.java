package stream.service;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;


import java.util.Comparator;

import stream.domain.*;

import java.util.List;
import java.util.Map;

import java.util.stream.*;

public class UserService {

    public static List<User> findUsersWhoHaveMoreThanOneAddress(List<User> users) {
        return users.stream()
                .filter((User c) -> c.getPersonDetails().getAddresses().size() > 1)
                .collect(Collectors.toList());
    }

    public static Person findOldestPerson(List<User> users) {

        return users.stream().map(c -> c.getPersonDetails())
                .collect(Collectors
                        .maxBy(Comparator
                                .comparing((person) -> person.getAge())))
                .get();
    }

    public static User findUserWithLongestUsername(List<User> users) {
        return users.stream()
                .max(Comparator.comparing((person) -> person.getName()))
                .get();
    }

    public static String getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(List<User> users) {

        return users.stream().map(p -> p.getPersonDetails())
                .filter(p -> p.getAge() < 18)
                .map(entry -> entry.getName() + " " + entry.getSurname())
                .collect(Collectors.joining(", "));

    }

    public static List<String> getSortedPermissionsOfUsersWithNameStartingWithA(List<User> users) {


        //

        return users.stream()
                .filter(p -> p.getName().startsWith("A"))
                .map(p -> p.getPersonDetails().getRole().getPermissions())
                .flatMap(p -> p.stream())
                //     lub p.getName
                .map(p -> p.toString()).sorted().collect(Collectors.toList());

    }

    public static void printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS(List<User> users) {
        users.stream()
                .filter(p -> p.getPersonDetails().getSurname().startsWith("S"))
                .map(p -> p.getPersonDetails().getRole().getPermissions())
                .flatMap(p -> p.stream())
                .map(p -> p.getName().toUpperCase())
                .forEach(p -> System.out.print(p + "\n"));
    }

    public static Map<Role, List<User>> groupUsersByRole(List<User> users) {
        return users.stream().collect(Collectors.groupingBy(p -> p.getPersonDetails().getRole()));
    }


    public static Map<Boolean, List<User>> partitionUserByUnderAndOver18(List<User> users) {
        return users.stream().collect(Collectors.partitioningBy(p -> p.getPersonDetails().getAge() > 18 ? TRUE : FALSE));

    }


}
