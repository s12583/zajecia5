package stream.service;

import junit.framework.TestCase;
import stream.domain.Person;
import stream.domain.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static stream.service.UserService.findOldestPerson;
import static stream.service.UserService.findUserWithLongestUsername;
import static stream.service.UserService.findUsersWhoHaveMoreThanOneAddress;

public class UserServiceTest extends TestCase {





    public void testFindUsersWhoHaveMoreThanOneAddress() throws Exception
    {
        List<User> testList = new ArrayList<>();

        testList.add(implementationUser.createUser1());
        testList.add(implementationUser.createUser2());


        assertEquals(findUsersWhoHaveMoreThanOneAddress(testList), testList);

    }

    public void testFindOldestPerson() throws Exception
    {
        List<User> testList = new ArrayList<>();

        testList.add(implementationUser.createUser1());
        testList.add(implementationUser.createUser2());

        assertEquals(implementationUser.createUser1().getPersonDetails().getAge(),findOldestPerson(testList).getAge());
    }

    public void testFindUserWithLongestUsername() throws Exception
    {
        List<User> testList = new ArrayList<>();

        testList.add(implementationUser.createUser1());
        testList.add(implementationUser.createUser2());


        assertEquals(findUserWithLongestUsername(testList).getName(), implementationUser.createUser2().getName() );
    }

    public void testGetNamesAndSurnamesCommaSeparatedOfAllUsersAbove18() throws Exception
    {
        
    }

    public void testGetSortedPermissionsOfUsersWithNameStartingWithA() throws Exception {
    }

    public void testPrintCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS() throws Exception {
    }

    public void testGroupUsersByRole() throws Exception {
    }

    public void testPartitionUserByUnderAndOver18() throws Exception {
    }

}